package com.epsi.fr.websitesb.websitesb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epsi.fr.websitesb.dal.ProductDao;


@Controller
public class ProductController {

	@Autowired
	private ProductDao productDao;

	@RequestMapping(value = { "/index", "/" })
	public String index(Model model) {
		model.addAttribute("page", "Product index");
		model.addAttribute("items", productDao.findAll());
		return "product/index";
	}

}
