package com.epsi.fr.websitesb.dal;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epsi.fr.websitesb.bo.User;

public interface UserDao extends JpaRepository<User, Long> {
	
}
