package com.epsi.fr.websitesb.websitesb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.epsi.fr.websitesb.dal" })

public class WebsitesbApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsitesbApplication.class, args);
	}

}
