package com.epsi.fr.websitesb.dal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.epsi.fr.websitesb.bo.Product;

@Component
public interface ProductDao extends JpaRepository<Product, Long>{
	
}
